#!/usr/bin/env python3
# Copyright (C) 2023  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU Affero General Public License version 3, or any later version
# See top-level LICENSE file for more information

import datetime
import json
from pathlib import Path
import sys

DISMISS_DB = Path("~/.config/gitlab-revq.json").expanduser()

def main():
    if len(sys.argv) == 1:
        print(f"Syntax: {sys.argv[0]} <url1> <url2> ...", file=sys.stderr)
        exit(1)

    now = datetime.datetime.now(tz=datetime.timezone.utc).isoformat()

    if DISMISS_DB.exists():
        with DISMISS_DB.open("r") as fd:
            db = json.load(fd)
    else:
        DISMISS_DB.touch()
        db = {"dismissed": []}

    for url in sys.argv[1:]:
        db["dismissed"].append({"webUrl": url, "at": now})

    tmp_db_path = Path(f"{DISMISS_DB}.tmp")
    with tmp_db_path.open("w") as fd:
        json.dump(db, fd)
    tmp_db_path.replace(DISMISS_DB)

if __name__ == "__main__":
    main()
