#!/usr/bin/env python3
# Copyright (C) 2023  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU Affero General Public License version 3, or any later version
# See top-level LICENSE file for more information

import configparser
import datetime
import itertools
import json
import multiprocessing.dummy
import os
from pathlib import Path
from pprint import pprint
import random
import re
import subprocess
import sys
import textwrap
import urllib.error
import urllib.request

from colorama import Fore, Style

try:
    import tqdm
except ImportError:
    tqdm = None


DEBUG = False
CONFIG_PATHS = [
    Path("~/.python-gitlab.cfg"),
    Path("/etc/python-gitlab.cfg"),
]
if "PYTHON_GITLAB_CFG" in os.environ:
    CONFIG_PATHS.insert(0, Path(os.environ["PYTHON_GITLAB_CFG"]))

DISMISS_DB = Path("~/.config/gitlab-revq.json").expanduser()

GITLAB_URL = "https://gitlab.softwareheritage.org/"
HIDDEN_USERNAMES = {"swh-public-ci", "phabricator-migration", "jenkins"}

PROJECTS_PER_REQUEST = 30
PUSH_RE = re.compile(r"added \d+ commit")


GRAPHQL_QUERY_PROJECTS = """
query($after: String) {
  projects(first: 100, after: $after) {
    pageInfo {
      endCursor
      startCursor
    }
    nodes {
      id
    }
  }
}
"""


GRAPHQL_QUERY = """
query($ids: [ID!], $after: String) {
  projects(ids: $ids, after: $after) {
    pageInfo {
      endCursor
      startCursor
    }
    nodes {
      name
      mergeRequests(state: opened, draft: false, sort: CREATED_DESC, first: 80) {
        nodes {
          createdAt
          title
          webUrl
          description
          approvedBy(first: 5) {
            nodes {
              username
            }
          }
          author {
            username
          }
          assignees(first: 10) {
            nodes {
              username
            }
          }
          reviewers(last: 20) {
            nodes {
              username
            }
          }
          mergeable
          notes(last: 20) {
            nodes {
              author {
                username
              }
              createdAt
              body
              system
            }
          }
          participants(last: 50) {
            nodes {
              username
            }
          }
          pipelines(last: 2) {
            nodes {
              status
            }
          }
          commits(last: 1) {
            nodes {
              authoredDate
            }
          }
        }
      }
    }
  }
}
"""

MIN_DATE = datetime.datetime.min.replace(tzinfo=datetime.timezone.utc)


def parse_date(s):
    return datetime.datetime.fromisoformat(s.replace("Z", "+00:00"))


def format_date(s):
    if s == "1900-01-01":
        return "never"
    else:
        assert s.endswith("Z")
        d = parse_date(s)
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        dt = now - d

        for (threshold, color) in [
            (60 * 60 * 24 * 7, Fore.RED),
            (60 * 60 * 24 * 2, Fore.BLUE),
            (60 * 60 * 4, Fore.YELLOW),
            (0, Fore.GREEN),
        ]:
            if int(dt.total_seconds()) > threshold:
                break
        for (ratio, unit) in [
            (60 * 60 * 24 * 7, "week"),
            (60 * 60 * 24, "day"),
            (60 * 60, "hour"),
            (60, "minute"),
            (1, "second"),
        ]:
            nb = int(dt.total_seconds()) // ratio
            if nb > 0:
                plural = "s" if nb > 1 else ""
                return f"{color}{s} ({nb} {unit}{plural} ago){Style.RESET_ALL}"

        return f"{Fore.CYAN}{s} (in the future){Style.RESET_ALL}"


def format_note(s):
    if s.startswith(">>! In !"):
        # Reply comment, migrated from Phabricator
        s = "\n".join(itertools.dropwhile(lambda l: l.startswith(">"), s.split("\n")))

    # MR migrated from Phabricator, strip the footer
    s = s.split("\n\n----\n*Migrated from D", 1)[0]

    s = s.strip()

    if s.count("\n") > 10:
        s = "\n".join(s.split("\n")[0:10]) + "\n[...]"

    return s


def is_system_note(note):
    return note[
        "system"
    ] and not PUSH_RE.match(  # exclude auto-generate messages, unless...
        note["body"].split("\n")[0]
    )  # ... it's a push


def main():
    config = configparser.ConfigParser()
    for path in CONFIG_PATHS:
        if path.expanduser().exists():
            config.read(path.expanduser())

    if len(sys.argv) == 1:
        instance = config["global"]["default"]
    elif len(sys.argv) == 2:
        (_, instance) = sys.argv
    else:
        print(f"Syntax: {sys.argv[0]} <instance_name>", file=sys.stderr)
        exit(1)

    if instance not in config:
        print(f"Invalid instance: {instance}", file=sys.stderr)
        exit(1)

    if DISMISS_DB.exists():
        with DISMISS_DB.open() as fd:
            db = json.load(fd)
    else:
        db = {"dismissed": []}

    data = graphql(config[instance], "query { currentUser { username }}")
    my_username = data["data"]["currentUser"]["username"]

    projects = []
    end_cursor = None

    it = itertools.count(1)

    if tqdm:
        it = tqdm.tqdm(it, desc="Fetching project list")

    for i in it:
        if not tqdm:
            print(f"Fetching projects page {i}...")
        page = graphql(config[instance], GRAPHQL_QUERY_PROJECTS, after=end_cursor)
        if "data" not in page:
            print(page)
            exit(1)
        for node in page["data"]["projects"]["nodes"]:
            projects.append(node["id"])
        end_cursor = page["data"]["projects"]["pageInfo"]["endCursor"]
        if end_cursor is None:
            break

    data = []

    def add_projects(ids):
        end_cursor = None
        for i in itertools.count(1):
            page = graphql(config[instance], GRAPHQL_QUERY, ids=ids, after=end_cursor)
            if "data" not in page:
                print(page)
                exit(1)
            data.extend(page["data"]["projects"]["nodes"])
            end_cursor = page["data"]["projects"]["pageInfo"]["endCursor"]

            if end_cursor is None:
                break
            if len(page["data"]["projects"]["nodes"]) == len(ids):
                # for some reason, cursor isn't None, but we are sure the next page is
                # empty.
                break

    random.shuffle(projects)

    project_batches = list(
        itertools.zip_longest(*([iter(projects)] * PROJECTS_PER_REQUEST))
    )
    with multiprocessing.dummy.Pool(10) as p:
        if tqdm:
            for _ in tqdm.tqdm(
                p.imap_unordered(add_projects, project_batches),
                total=len(project_batches),
                desc="Fetching MRs",
            ):
                pass
        else:
            for (i, _) in enumerate(p.imap_unordered(add_projects, project_batches)):
                print(f"Got MRs page {i}.")

    merge_requests = [
        {
            **merge_request,
            "project_name": project["name"],
            "author": merge_request["author"]["username"],
            "approvedBy": [
                approved_by["username"]
                for approved_by in merge_request["approvedBy"]["nodes"]
            ],
            "assignees": [
                approved_by["username"]
                for approved_by in merge_request["assignees"]["nodes"]
            ],
            "participants": [
                approved_by["username"]
                for approved_by in merge_request["participants"]["nodes"]
            ],
            "reviewers": [
                approved_by["username"]
                for approved_by in merge_request["reviewers"]["nodes"]
            ],
            "pipelines": merge_request["pipelines"]["nodes"]
            if merge_request["pipelines"]
            else [],
            "commits": merge_request["commits"]["nodes"],
            "notes": [
                {**note, "author": note["author"]["username"]}
                for note in merge_request["notes"]["nodes"]
                if note is not None
            ],
        }
        for project in data
        for merge_request in project["mergeRequests"]["nodes"]
    ]

    if DEBUG:
        pprint(merge_requests)
        print("=" * 100)

    for merge_request in merge_requests:
        merge_request["last_activity"] = max(
            parse_date(merge_request["createdAt"]),
            max(
                (
                    parse_date(note["createdAt"])
                    for note in merge_request["notes"]
                    if note["author"] not in HIDDEN_USERNAMES
                    and not is_system_note(note)
                ),
                default=MIN_DATE,
            ),
            max(
                (
                    parse_date(commit["authoredDate"])
                    for commit in merge_request["commits"]
                ),
                default=MIN_DATE,
            ),
        )

    ignores = {ignore["webUrl"]: parse_date(ignore["at"]) for ignore in db["dismissed"]}
    merge_requests = [
        merge_request
        for merge_request in merge_requests
        if merge_request["webUrl"] not in ignores
        or merge_request["last_activity"] > ignores[merge_request["webUrl"]]
    ]

    # Sort by last activity
    merge_requests.sort(key=lambda merge_request: merge_request["last_activity"])

    for merge_request in merge_requests:
        if merge_request["author"] == my_username:
            # Don't display my own MRs
            continue
        if my_username in merge_request["approvedBy"]:
            # I already approved it
            continue

        last_commit_date = max(
            (commit["authoredDate"] for commit in merge_request["commits"]),
            default="1900-01-01",
        )
        others_last_note = max(
            (
                note
                for note in merge_request["notes"]
                if note["author"] not in {my_username, *HIDDEN_USERNAMES}
                and not is_system_note(note)
            ),
            key=lambda note: note["createdAt"],
            default=None,
        )
        others_last_note_date = (
            others_last_note["createdAt"] if others_last_note else "1900-01-01"
        )
        my_last_note = max(
            (note for note in merge_request["notes"] if note["author"] == my_username),
            key=lambda note: note["createdAt"],
            default=None,
        )
        my_last_note_date = my_last_note["createdAt"] if my_last_note else "1900-01-01"

        if (
            last_commit_date < my_last_note_date
            and others_last_note_date < my_last_note_date
        ):
            # No new commit or note since my last note
            continue

        if DEBUG:
            pprint(merge_request)
        print(
            f"{merge_request['project_name']}: {Style.BRIGHT}{merge_request['title']}{Style.RESET_ALL} {merge_request['webUrl']}"
        )
        print(f"\tby                 {merge_request['author']}")
        if merge_request["approvedBy"]:
            print(
                f"\t{Fore.GREEN}approved{Style.RESET_ALL} by:       {' '.join(merge_request['approvedBy'])}"
            )
        if not merge_request["mergeable"]:
            print(f"\t{Fore.RED}Not mergeable{Style.RESET_ALL}")

        if merge_request["pipelines"]:
            pipeline_status = merge_request["pipelines"][-1]["status"]
        else:
            pipeline_status = "NONE"
        if pipeline_status in ("FAILED", "CANCELED"):
            color = Fore.RED
        elif pipeline_status == "SUCCESS":
            color = Fore.GREEN
        else:
            color = Fore.BLUE
        print(f"\tPipeline:          {color}{pipeline_status}{Style.RESET_ALL}")

        participants = (
            set(merge_request["participants"])
            - HIDDEN_USERNAMES
            - {merge_request["author"]}
        )
        if participants:
            print(f"\tParticipants:      {' '.join(sorted(participants))}")
        print(f"\tcreated at:        {format_date(merge_request['createdAt'])}")
        description = format_note(merge_request["description"] or "")
        if description:
            print(
                "\t\t"
                + description.split("\n", 1)[0]
                + (" [...]" if "\n" in description else "")
            )
        print(f"\tlast commit:       {format_date(last_commit_date)}")
        if my_last_note:
            print(f"\tmy last note:      {format_date(my_last_note_date)}")
            print(textwrap.indent(format_note(my_last_note["body"]), "\t\t"))
        else:
            print(f"\tmy last note:      {format_date(my_last_note_date)}")
        if others_last_note:
            print(
                f"\tothers' last note: {format_date(others_last_note_date)} by {others_last_note['author']}"
            )
            print(textwrap.indent(format_note(others_last_note["body"]), "\t\t"))
        else:
            print(f"\tothers' last note: never")
        print()


def graphql(config, query, **kwargs):

    graphql_url = config["url"].strip("/") + "/api/graphql"
    api_token = config["private_token"].strip()

    if api_token.startswith("helper: "):
        api_token = subprocess.check_output(api_token[8:], shell=True).strip().decode()

    req = urllib.request.Request(
        graphql_url,
        data=json.dumps({"query": query, "variables": kwargs}).encode(),
        headers={
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_token}",
        },
    )

    try:
        with urllib.request.urlopen(req) as resp:
            return json.load(resp)
    except urllib.error.HTTPError as e:
        print(e.read())
        raise


if __name__ == "__main__":
    main()
