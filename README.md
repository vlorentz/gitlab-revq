# gitlab-revq

A CLI tool to visualize your review queue

## How to use

1. [Get an API token](https://gitlab.softwareheritage.org/-/profile/personal_access_tokens)
2. Write it to `~/.python-gitlab.cfg`:
   ```
   [global]
   default = swh
   ssl_verify = true
   timeout = 5
   
   [swh]
   url = https://gitlab.softwareheritage.org/
   private_token = <REDACTED>
   api_version = 4
   ```
3. `pip3 install colorama tqdm`  (tqdm is optional)
4. Run `./revq.py`

You can dismiss (hide) a merge request until someone acts on it again
with: `./dismiss.py <url>`

![screenshot](https://gitlab.softwareheritage.org/vlorentz/gitlab-revq/-/raw/assets/screenshot.png)
